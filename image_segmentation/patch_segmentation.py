import h5py
import numpy as np
from scipy import ndimage as ndi
from skimage.morphology import watershed
from skimage.feature import peak_local_max
import matplotlib.pyplot as plt
from skimage.filters import threshold_otsu, sobel
from skimage.morphology import closing, square, opening, disk, erosion, reconstruction, dilation
from scipy.misc import imresize
from copy import deepcopy
import sys
from skimage import measure
import math
from skimage import img_as_float
from scipy.ndimage import gaussian_filter
from scipy.ndimage.filters import maximum_filter
from scipy.ndimage.morphology import generate_binary_structure, binary_erosion
from skimage.measure import label
from skimage.segmentation import slic
from skimage.future import graph
from skimage import data
from skimage import segmentation
from skimage.filters import rank
from skimage import feature


def load_images(filename):
    key_name = "images"
    images = []
    with h5py.File(filename) as f:
        image_set = f[key_name]
        for image in image_set:
            images.append(image)
    return np.array(images)


def get_peak_based_labels(image):

    # Threshold to binarize image
    thresh = threshold_otsu(image)    
    bw = image >= thresh
    
    # Calculate distance transform for watershed
    distance = ndi.distance_transform_edt(bw)

    # Calculate local maxima, TODO experiment with footprint size
    local_maxi = peak_local_max(distance, indices=False,
                                footprint=np.ones((35, 35)),
                                labels=bw)

    # Check for corner case of no peaks found
    im_width, im_height = image.shape
    if np.sum(local_maxi == False) == im_width * im_height:
        return []
    #print(np.sum(local_maxi == True))
    
    markers = ndi.label(local_maxi)[0]
    labels = watershed(-distance, markers, mask=bw)

    return labels


def get_elevation_based_labels(image):

    #seed = np.copy(image)
    #seed[1:-1, 1:-1] = image.max()
    #mask = image
    #eroded = reconstruction(seed, mask, method='erosion')
    
    seed = np.copy(image)
    seed[1:-1, 1:-1] = image.min()
    mask = image
    dilated = reconstruction(seed, mask, method='dilation')
    hdome = image - dilated
    
    elevation_map = sobel(hdome)
    elevation_thresh = threshold_otsu(elevation_map)
    markers = np.where(elevation_map >= elevation_thresh, 2, 0)
    
    pixel_values = sorted(hdome.flatten())
    lower_thresh = int(np.percentile(pixel_values, .3))
    markers[lower_thresh] = 1

    segmentation = watershed(elevation_map, markers)

    selem = disk(6)
    labels = opening(segmentation, selem)
    return label(labels)


def watershed_segmentation(image, img_number):

    im_width, im_height = image.shape

    labels = get_elevation_based_labels(image)

    #plt.imshow(labels, interpolation='nearest')
    #plt.show()

    # Map regions to coordinates
    box_coords = {}
    d, k = labels.shape
    for i in range(d):
        for j in range(k):
            if labels[i][j] in box_coords:
                box_coords[labels[i][j]].append((i,j))
            else:
                box_coords[labels[i][j]] = [(i,j)]
    
    # Generating new cropped images
    new_images = []
    max_len_region = max([len(val) for val in box_coords.values()])
    
    for key in box_coords.keys():
        region = box_coords[key]
        # Ignore the background tag
        if len(region) == max_len_region:
            continue
        # Found Basin
        xs, ys = zip(*region)
        minx = min(xs)
        maxx = max(xs)
        miny = min(ys)
        maxy = max(ys)
        #print(minx, maxx, miny, maxy)
        optimums = fix_corner_nuclei(minx, maxx, miny, maxy, im_width, im_height)
        (minx, maxx, miny, maxy) = optimums

        if (maxx - minx) < 50 and (maxy - miny) < 50:
            continue

        new_image = deepcopy(image[minx:maxx+1, miny:maxy+1])

        area = 0.0
        # Zero out non-key region pixels
        region_keys = set(region)
        for i in range(minx, maxx + 1):
            for j in range(miny, maxy + 1):
                if (i, j) not in region_keys:
                    new_image[i - minx, j - miny] = 0.0
                else:
                    area += 1.
        total = (maxx - minx) * (maxy - miny)

        print("Region area: ", area, total)
        new_images.append((new_image, area))

        print("Nuclei Number:", img_number + len(new_images),
              "Rectangle", minx, maxx, miny, maxy)

    return new_images


def centered_zero_pad(image):
    r, c = image.shape
    max_length = max(r, c)
    min_length = min(r, c)
    shift = int(abs(r - c) / 2)
    new_image = np.zeros((max_length, max_length))
    if r == min_length:
        new_image[shift: shift + min_length] = image
    else:
        new_image[:, shift: shift + min_length] = image
    return new_image


def fix_corner_nuclei(minx, maxx, miny, maxy, width, height):
    # width should be 0 - 511, height should be 0 - 511
    padding = 15  # Nuclei should be 15 pixels within image
    bad_top_edge = 0 <= minx < padding
    bad_bottom_edge = width - padding <= maxx < width
    bad_left_edge = 0 <= miny < padding
    bad_right_edge = height - padding <= maxy < height

    if bad_top_edge:
        minx = padding
    if bad_bottom_edge:
        maxx = width - padding
    if bad_left_edge:
        miny = padding
    if bad_right_edge:
        maxy = height - padding

    return (minx, maxx, miny, maxy)


def generate_crops(image_set):
    cropped_imgs = []
    count = 0

    for image in image_set:
        count += 1
        if np.max(image) >= 1000:
            new_crops = watershed_segmentation(image, len(cropped_imgs))
            for img in new_crops:
                cropped_imgs.append(img)

        print("PROGRESS", count, len(image_set))
        #if count == 100:
        #    break
    cropped_imgs, large_imgs = pad_crops(cropped_imgs)
    patches = []
    for crop in cropped_imgs:        
        new_patches = get_patches(crop)
        if len(new_patches) > 0:
            patches += new_patches
    #for img in large_imgs:
    #    new_patches = get_patches(img)
    #    if len(new_patches) > 0:
    #        patches += new_patches


    return np.array(patches)

def get_patches(crop):
    patch_size = 32
    r, c = crop.shape
    new_patches = []
    j = 0
    while j < r - patch_size:
        i = 0
        while i < c - patch_size:
            new_patch = deepcopy(crop[i:i+patch_size, j:j+patch_size])
            area = np.sum(new_patch > 0)
            if area >= .8 * patch_size * patch_size:
                if overexposed_image(new_patch, area):
                    print("OVEREXPOSED PATCH")
                else:
                    new_patches.append(new_patch)
            i += int(patch_size / 2)
        j += int(patch_size / 2)

    return new_patches

def pad_crops(cropped_images):
    new_crops = []
    avg_area = sum([area for (image, area) in cropped_images]) / len(cropped_images)
    std = sum([(area - avg_area) ** 2 for (image, area) in cropped_images]) / len(cropped_images)
    std = math.sqrt(std)
    print(avg_area, std)
    max_r = 0.0
    max_c = 0.0
    crop_r = 128
    crop_c = 128
    areas = [area for (image, area) in cropped_images]
    #n, bins, patches = plt.hist(areas, 50, facecolor='green', alpha=0.75)
    #plt.show()
    good_crops = []
    large_images = []
    for image, area in cropped_images:
        z_score = (area- avg_area) / std
        #if abs(z_score) >= 2:
        #    continue
        if area <= 2000:  # cut out small bright spots
            continue
        else:
            r, c = image.shape
            if r > crop_r or c > crop_c:
                large_images.append(image)
                continue
            if overexposed_image(image, area):
                print("OVEREXPOSED")
                continue
            max_r = max(max_r, r)
            max_c = max(max_c, c)
            good_crops.append(image)
    #print(max_r, max_c)

    for image in good_crops:
        new_crops.append(pad_nuclei(image, crop_r, crop_c))
        #new_crops.append(pad_nuclei(image, max_r, max_c))

    return new_crops, large_images


def overexposed_image(image, area):
    # working with 12 bit images
    threshold = 3700
    exposure_filter = np.where(image > threshold, 1., 0.)
    if np.sum(exposure_filter) >= .25 * area:
        return True
    return False


def pad_nuclei(image, max_r, max_c):
    r, c = image.shape
    shift_r = int((max_r - r) / 2)
    shift_c = int((max_c - c) / 2)

    new_image = np.zeros((max_r, max_c))
    new_image[shift_r: shift_r + r, shift_c: shift_c + c] = image

    return new_image


def main(args):

    fname = args[1]
    image_set = load_images(fname)
    cropped_images = generate_crops(image_set)
    tokens = fname.split("_")
    oname = tokens[0] + "_" + tokens[1] + "_" + tokens[2] +  "_" + tokens[3] + "_crops.hdf5"

    with h5py.File(oname, "w") as f:
        f.create_dataset('images', data=cropped_images)



if __name__ == "__main__":
    main(sys.argv)
