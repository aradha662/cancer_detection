import h5py
import numpy as np
from scipy import ndimage as ndi
from skimage.morphology import watershed
from skimage.feature import peak_local_max
import matplotlib.pyplot as plt
from skimage.filters import threshold_otsu, sobel
from skimage.morphology import closing, square, opening, disk
from skimage import segmentation
from scipy.misc import imresize
from copy import deepcopy
import sys
from skimage import measure
import math
import pickle

def load_images(filename):
    key_name = "images"
    images = []
    with h5py.File(filename) as f:
        image_set = f[key_name]
        for image in image_set:
            images.append(image)
    return np.array(images)


def get_peak_based_labels(image):

    # Threshold to binarize image
    thresh = threshold_otsu(image)
    bw = closing(image > thresh, square(3))

    # Calculate distance transform for watershed
    distance = ndi.distance_transform_edt(bw)

    # Calculate local maxima, TODO experiment with footprint size
    local_maxi = peak_local_max(distance, indices=False,
                                footprint=np.ones((60, 60)),
                                labels=bw)

    # Check for corner case of no peaks found
    im_width, im_height = image.shape
    if np.sum(local_maxi == False) == im_width * im_height:
        return []
    print(np.sum(local_maxi == True))

    markers = ndi.label(local_maxi)[0]
    labels = watershed(-distance, markers, mask=bw)

    return labels


def get_elevation_based_labels(image):
    elevation_map = sobel(image)
    elevation_thresh = threshold_otsu(elevation_map)
    markers = np.where(elevation_map > elevation_thresh, 2, 0)

    pixel_values = sorted(image.flatten())
    lower_thresh = int(np.percentile(pixel_values, .3))
    markers[lower_thresh] = 1

    labels = watershed(elevation_map, markers)

    return labels


def get_connected_comp_coords(labels):
    r, c = labels.shape
    coord_set = set([])
    regions = []
    for i in range(r):
        for j in range(c):
            if (i, j) not in coord_set and labels[i, j] == 2:
                region, coord_set = grow_region(labels, (i, j), coord_set)
                regions.append(region)
    return regions


def grow_region(labels, start, coord_set):
    agenda = [start]
    r, c = labels.shape
    region = []

    while len(agenda) > 0:
        (row, col) = agenda.pop(0)
        coord_set.add((row, col))
        region.append((row, col))
        neighbors = []
        #print(len(agenda))
        #print(coord_set)
        if row > 0:
            neighbors.append((row - 1, col))
        if col > 0:
            neighbors.append((row, col - 1))
        if row < r - 1:
            neighbors.append((row + 1, col))
        if col < c - 1:
            neighbors.append((row, col + 1))
        if row > 0 and col < c - 1:
            neighbors.append((row - 1, col + 1))
        if row > 0 and col > 0:
            neighbors.append((row - 1, col - 1))
        if row < r - 1 and col < c - 1:
            neighbors.append((row + 1, col + 1))
        if row < r - 1 and col > 0:
            neighbors.append((row + 1, col - 1))

        for neighbor in neighbors:
            (new_r, new_c) = neighbor
            if labels[new_r, new_c] == 2:
                if neighbor in coord_set:
                    continue
                elif neighbor not in agenda:
                    agenda.append(neighbor)

    return region, coord_set


def watershed_segmentation(image, img_number):

    im_width, im_height = image.shape

    labels = get_elevation_based_labels(image)
    #labels = get_peak_based_labels(image)

    selem = disk(6)
    labels = opening(labels, selem)

    #plt.imshow(labels, interpolation='nearest')
    #plt.show()


    # Extract regions as coordinates
    box_coords = get_connected_comp_coords(labels)

    """
    # Map regions to coordinates
    box_coords = {}
    d, k = labels.shape
    for i in range(d):
        for j in range(k):
            if labels[i][j] in box_coords:
                box_coords[labels[i][j]].append((i,j))
            else:
                box_coords[labels[i][j]] = [(i,j)]

    print(box_coords.keys())
    """

    # Generating new cropped images
    new_images = []
    #im_size = (256, 256)  # Can alter scaling size if needed

    for region in box_coords:
        # Found Basin
        xs, ys = zip(*region)
        minx = min(xs)
        maxx = max(xs)
        miny = min(ys)
        maxy = max(ys)
        #print(minx, maxx, miny, maxy)
        optimums = fix_corner_nuclei(minx, maxx, miny, maxy, im_width, im_height)
        (minx, maxx, miny, maxy) = optimums

        if (maxx - minx) < 50 and (maxy - miny) < 50:
            continue

        new_image = deepcopy(image[minx:maxx+1, miny:maxy+1])

        area = 0.0
        # Zero out non-key region pixels
        for i in range(minx, maxx + 1):
            for j in range(miny, maxy + 1):
                if (i, j) not in region:
                    new_image[i - minx, j - miny] = 0.0
                else:
                    area += 1.
        total = (maxx - minx) * (maxy - miny)

        print("Region area: ", area, total)

        #    continue

        #new_image = centered_zero_pad(new_image)
        #new_image = imresize(new_image,
        #                     im_size)
        new_images.append((new_image, area))

        print("Nuclei Number:", img_number + len(new_images),
              "Rectangle", minx, maxx, miny, maxy)

    return new_images


def centered_zero_pad(image):
    r, c = image.shape
    max_length = max(r, c)
    min_length = min(r, c)
    shift = int(abs(r - c) / 2)
    new_image = np.zeros((max_length, max_length))
    if r == min_length:
        new_image[shift: shift + min_length] = image
    else:
        new_image[:, shift: shift + min_length] = image
    return new_image


def fix_corner_nuclei(minx, maxx, miny, maxy, width, height):
    # width should be 0 - 511, height should be 0 - 511
    padding = 15  # Nuclei should be 15 pixels within image
    bad_top_edge = 0 <= minx < padding
    bad_bottom_edge = width - padding <= maxx < width
    bad_left_edge = 0 <= miny < padding
    bad_right_edge = height - padding <= maxy < height

    if bad_top_edge:
        minx = padding
    if bad_bottom_edge:
        maxx = width - padding
    if bad_left_edge:
        miny = padding
    if bad_right_edge:
        maxy = height - padding

    return (minx, maxx, miny, maxy)


def generate_crops(image_set):
    cropped_imgs = []
    count = 0

    for image in image_set:
        count += 1
        if np.max(image) >= 1000:  # Get rid of images with no nuclei
            new_crops = watershed_segmentation(image, len(cropped_imgs))
            for img in new_crops:
                cropped_imgs.append(img)

        print("PROGRESS", count, len(image_set))
        if count == 100:
            break

    cropped_imgs, large_imgs = pad_crops(cropped_imgs)
    """
    fixed_imgs = []
    f = open("labels.p", "wb")
    for image in large_imgs:
        new_image = reprocess_image(image)
        fixed_imgs.append(new_image)
    pickle.dump(fixed_imgs, f)
    """
    return cropped_imgs


def reprocess_image(image):

    labels = get_peak_based_labels(image)

    selem = disk(6)
    labels = opening(labels, selem)

    #plt.imshow(labels, interpolation='nearest')
    #plt.show()

    # Map regions to coordinates
    box_coords = {}
    d, k = labels.shape
    for i in range(d):
        for j in range(k):
            if labels[i][j] in box_coords:
                box_coords[labels[i][j]].append((i,j))
            else:
                box_coords[labels[i][j]] = [(i,j)]

    print("KEYS:", box_coords.keys())

    return labels


def pad_crops(cropped_images):
    new_crops = []
    avg_area = sum([area for (image, area) in cropped_images]) / len(cropped_images)
    std = sum([(area - avg_area) ** 2 for (image, area) in cropped_images]) / len(cropped_images)
    std = math.sqrt(std)
    print(avg_area, std)
    max_r = 0.0
    max_c = 0.0
    crop_r = 128
    crop_c = 128
    areas = [area for (image, area) in cropped_images]
    #n, bins, patches = plt.hist(areas, 50, facecolor='green', alpha=0.75)
    #plt.show()
    good_crops = []
    tossed_images = []
    large_images = []
    for image, area in cropped_images:
        z_score = (area- avg_area) / std
        if abs(z_score) >= 2:
            tossed_images.append((image, "AREA TOO LARGE"))
            large_images.append(image)
            continue
        elif area <= 2000:  # cut out small bright spots
            tossed_images.append((image, "AREA TOO SMALL"))
            continue
        else:
            r, c = image.shape
            if r > crop_r or c > crop_c:
                tossed_images.append((image, "MARGINS TOO LARGE"))
                large_images.append(image)
                continue
            if overexposed_image(image, area):
                tossed_images.append((image, "OVEREXPOSED"))
                print("OVEREXPOSED")
                continue
            max_r = max(max_r, r)
            max_c = max(max_c, c)
            good_crops.append(image)
    #print(max_r, max_c)
    f = open("tossed_images.p", 'wb')
    pickle.dump(tossed_images, f)
    for image in good_crops:
        new_crops.append(pad_nuclei(image, crop_r, crop_c))
        #new_crops.append(pad_nuclei(image, max_r, max_c))

    return new_crops, large_images


def overexposed_image(image, area):
    # working with 12 bit images
    threshold = 3700
    exposure_filter = np.where(image > threshold, 1., 0.)
    if np.sum(exposure_filter) >= .25 * area:
        return True
    return False


def pad_nuclei(image, max_r, max_c):
    r, c = image.shape
    shift_r = int((max_r - r) / 2)
    shift_c = int((max_c - c) / 2)

    new_image = np.zeros((max_r, max_c))
    new_image[shift_r: shift_r + r, shift_c: shift_c + c] = image

    return new_image


def main(args):

    fname = args[1]
    image_set = load_images(fname)
    cropped_images = generate_crops(image_set)
    tokens = fname.split("_")
    oname = tokens[0] + "_" + tokens[1] + "_" + tokens[2] +  "_" + tokens[3] + "_crops.hdf5"
    with h5py.File(oname, "w") as f:
        f.create_dataset('images', data=cropped_images)



if __name__ == "__main__":
    main(sys.argv)
