import options_parser
import os
import h5py
import numpy as np
import tensorflow as tf
import time

def weight_variable(name, shape):
    return tf.get_variable(name, shape=shape,
                           initializer=tf.contrib.layers.variance_scaling_initializer())

def bias_variable(shape):
    initial = tf.constant(0.01, shape=shape)
    return tf.Variable(initial)

def conv2d(x, W):
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

def max_pool(name, x, patch_size):

    with tf.name_scope(name):
        pool = tf.nn.max_pool(x, ksize=[1, patch_size, patch_size, 1],
                              strides=[1, patch_size, patch_size, 1], padding='SAME')
    return pool

def conv_module(name, inputs, kernel_size, num_input_channels,
                num_filters, train_flag):

    with tf.name_scope(name):
        W_conv = weight_variable(name,
                                 [kernel_size, kernel_size,
                                  num_input_channels, num_filters])
        b_conv = bias_variable([num_filters])
        """
        conv = tf.contrib.layers.batch_norm(lrelu(conv2d(inputs, W_conv) + b_conv),
                                            decay=0.9,
                                            epsilon=1e-5,
                                            scale=True,
                                            scope=name+"batch_norm",
                                            updates_collections=None,
                                            is_training=train_flag)
        """
        conv = prelu(conv2d(inputs, W_conv) + b_conv, name)

    return conv

# Fully Connected layer
def fc_module(name, inputs, input_size, output_size):

    W_fc = weight_variable(name, [input_size, output_size])
    b_fc = bias_variable([output_size])
    fc = tf.matmul(inputs, W_fc) + b_fc

    return fc


def lrelu(x, leak=0.01):
    return tf.maximum(x, leak * x)

def prelu(x, name):
    a = tf.Variable(0.25, name=name + "prelu")
    return tf.maximum(x, 0.0) + a * tf.minimum(x, 0.0)


def setup_model(x, target, imsize, is_training, keep_prob, weights):

    image_size = imsize
    x_image = tf.reshape(x, [-1, image_size, image_size, 1])

    # Image is initially 128 x 128
    kernel_sizes = [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3]
    filter_list = [64, 64, 128, 128, 256, 256, 256, 512, 512, 512, 512, 512, 512]
    pool_sizes = [2, 2, 2, 2, 2]  # Final image is 64 * 64
    num_hidden_units_1 = 4096
    num_hidden_units_2 = 4096
    num_output_units = 2

    norm_x = tf.contrib.layers.batch_norm(x_image,
                                          decay=0.9,
                                          epsilon=1e-5,
                                          scale=True,
                                          scope="First_batch_norm",
                                          updates_collections=None,
                                          is_training=is_training)

    conv_1_1 = conv_module("W_conv_1_1", norm_x, kernel_sizes[0],
                           1, filter_list[0], is_training)
    conv_1_2 = conv_module("W_conv_1_2", conv_1_1, kernel_sizes[1],
                           filter_list[0], filter_list[1], is_training)

    conv_2_1 = conv_module("W_conv_2_1", conv_1_2, kernel_sizes[2],
                           filter_list[1], filter_list[2], is_training)
    conv_2_2 = conv_module("W_conv_2_2", conv_2_1, kernel_sizes[3],
                           filter_list[2], filter_list[3], is_training)

    conv_3_1 = conv_module("W_conv_3_1", conv_2_2, kernel_sizes[4],
                           filter_list[3], filter_list[4], is_training)
    conv_3_2 = conv_module("W_conv_3_2", conv_3_1, kernel_sizes[5],
                           filter_list[4], filter_list[5], is_training)
    conv_3_3 = conv_module("W_conv_3_3", conv_3_2, kernel_sizes[6],
                           filter_list[5], filter_list[6], is_training)

    pool_3 = max_pool("pool_3", conv_3_3, pool_sizes[2])

    new_image_size = int(image_size / pool_sizes[2])

    conv_4_1 = conv_module("W_conv_4_1", pool_3, kernel_sizes[7],
                           filter_list[6], filter_list[7], is_training)
    conv_4_2 = conv_module("W_conv_4_2", conv_4_1, kernel_sizes[8],
                           filter_list[7], filter_list[8], is_training)
    conv_4_3 = conv_module("W_conv_4_3", conv_4_2, kernel_sizes[9],
                           filter_list[8], filter_list[9], is_training)

    pool_4 = max_pool("pool_4", conv_4_3, pool_sizes[3])

    new_image_size = int(new_image_size / pool_sizes[3])

    fc_input_dim = new_image_size * new_image_size * filter_list[-1]
    pool_flat = tf.reshape(pool_4,
                           [-1, fc_input_dim])

    fc_1 = tf.nn.relu(fc_module("W_fc_1", pool_flat, fc_input_dim, num_hidden_units_1), "W_fc_1")
    fc_1_drop = tf.nn.dropout(fc_1, keep_prob)
    fc_2 = tf.nn.relu(fc_module("W_fc_2", fc_1_drop, num_hidden_units_1, num_hidden_units_2))
    fc_2_drop = tf.nn.dropout(fc_2, keep_prob)

    logits = fc_module("W_fc_3", fc_2_drop, num_hidden_units_2, num_output_units)

    with tf.name_scope("loss"):
        softmax = tf.nn.softmax_cross_entropy_with_logits(logits, target)
        weighted_softmax = tf.mul(weights, softmax)
        softmax_loss = tf.reduce_mean(weighted_softmax)

    train_loss = tf.summary.scalar('training_loss', softmax_loss)
    val_loss = tf.summary.scalar('val_loss', softmax_loss)

    train_step = tf.train.AdamOptimizer(learning_rate=5e-5).minimize(softmax_loss)

    predictions = tf.nn.softmax(logits)
    correct_predictions = tf.equal(tf.argmax(predictions, 1), tf.argmax(target, 1))

    with tf.name_scope("accuracy"):
        accuracy = tf.reduce_mean(tf.cast(correct_predictions, tf.float32))

    train_acc = tf.summary.scalar('training accuracy', accuracy)
    val_acc = tf.summary.scalar('validation accuracy', accuracy)

    train_summary = tf.summary.merge([train_loss, train_acc])
    val_summary = tf.summary.merge([val_loss, val_acc])

    summaries = (train_summary, val_summary)

    return (train_step, softmax_loss, accuracy, summaries)


def load_data(dir_name, filenames):

    key_name = "images"
    images = []
    imsize = 0.0
    for filename in filenames:
        fname = dir_name + "/" + filename
        with h5py.File(fname) as f:
            image_set = f[key_name]
            for image in image_set:
                images.append(image)
                if imsize == 0.0:
                    imsize = image.shape[0]

    return np.array(images), imsize


def merge_and_split_data(pos_data, neg_data):
    #pos_data = pos_data # TODO
    #neg_data = neg_data # TODO
    pos_data = np.random.permutation(pos_data)
    neg_data = np.random.permutation(neg_data)

    pos_labels = np.zeros((len(pos_data), 2))
    neg_labels = np.zeros((len(neg_data), 2))

    pos_labels[:, 1] = 1.
    neg_labels[:, 0] = 1.

    total_examples = len(pos_labels) + len(neg_labels)
    pos_weights = (1. * total_examples) / len(pos_labels) * np.ones((len(pos_data), 1))
    neg_weights = (1. * total_examples) / len(neg_labels) * np.ones((len(neg_data), 1))

    pos_split_idx = int(len(pos_data) * .2)
    neg_split_idx = int(len(neg_data) * .2)
    split_idx = min(pos_split_idx, neg_split_idx)
    neg_split_idx = split_idx
    pos_split_idx = split_idx

    pos_val_merged = list(zip(pos_data[0:pos_split_idx],
                              pos_labels[0:pos_split_idx],
                              pos_weights[0:pos_split_idx]))

    neg_val_merged = list(zip(neg_data[0:neg_split_idx],
                              neg_labels[0:neg_split_idx],
                              neg_weights[0:neg_split_idx]))
    val_merge = np.random.permutation(pos_val_merged + neg_val_merged)

    pos_train_merged = list(zip(pos_data[pos_split_idx:],
                                pos_labels[pos_split_idx:],
                                pos_weights[pos_split_idx:]))
    neg_train_merged = list(zip(neg_data[neg_split_idx:],
                                neg_labels[neg_split_idx:],
                                neg_weights[neg_split_idx:]))

    train_merge = np.random.permutation(pos_train_merged + neg_train_merged)

    train_data = [d[0] for d in train_merge]
    train_labels = [d[1] for d in train_merge]
    train_weights = [d[2] for d in train_merge]
    val_data = [d[0] for d in val_merge]
    val_labels = [d[1] for d in val_merge]
    val_weights = [d[2] for d in val_merge]

    return train_data, val_data, train_labels, val_labels, train_weights, val_weights


def permute_train_data(train_data, train_labels, train_weights):
    merged = list(zip(train_data, train_labels, train_weights))
    permuted = np.random.permutation(merged)
    train_data = [d[0] for d in permuted]
    train_labels = [d[1] for d in permuted]
    train_weights = [d[2] for d in permuted]
    return train_data, train_labels, train_weights


def process_train_data(train_data, train_labels, train_weights, x, answer,
                       is_training, keep_prob, weights,
                       loss, accuracy, train_step, sess,
                       merged, train_writer):
    global train_batch_count
    batch_size = 32
    batch_idx = 0
    total_loss = 0.0
    acc = 0.0
    while batch_idx < len(train_data):
        end = batch_idx + batch_size
        if end > len(train_data):
            end = len(train_data)
        train_batch_count += 1
        step, batch_loss, batch_acc, summary= sess.run([train_step, loss, accuracy, merged],
                                                       feed_dict={x: train_data[batch_idx:end],
                                                                  answer: train_labels[batch_idx:end],
                                                                  is_training: True,
                                                                  keep_prob: .5,
                                                                  weights: train_weights[batch_idx:end]})

        total_loss += batch_loss * (end - batch_idx)
        acc += batch_acc * (end - batch_idx)
        train_writer.add_summary(summary, train_batch_count)

        batch_idx += batch_size
    return total_loss / len(train_data), acc / len(train_data)


def process_val_data(val_data, val_labels, val_weights, x, answer, is_training,
                     keep_prob, weights, accuracy, loss, sess,
                     merged, val_writer):

    global val_batch_count
    batch_size = 32
    batch_idx = 0
    total_loss = 0.0
    acc = 0.0
    while batch_idx < len(val_data):
        end = batch_idx + batch_size
        if end > len(val_data):
            end = len(val_data)
        val_batch_count += 1
        batch_loss, batch_acc, summary = sess.run([loss, accuracy, merged],
                                                  feed_dict={x: val_data[batch_idx:end],
                                                             answer: val_labels[batch_idx:end],
                                                             is_training: False,
                                                             keep_prob: 1.,
                                                             weights: val_weights[batch_idx:end]})
        total_loss += batch_loss * (end - batch_idx)
        acc += batch_acc * (end - batch_idx)
        val_writer.add_summary(summary, val_batch_count)
        batch_idx += batch_size

    return total_loss / len(val_data), acc / len(val_data)


def main(options):
    pos_label_dir = options.pos_label_dir
    neg_label_dir = options.neg_label_dir

    pos_files = os.listdir(pos_label_dir)
    neg_files = os.listdir(neg_label_dir)

    print(pos_files, neg_files)

    pos_image_data, pos_imsize = load_data(pos_label_dir, pos_files)  # Size is (?, 256, 256)
    neg_image_data, neg_imsize = load_data(neg_label_dir, neg_files)  # Size is (?, 256, 256)

    imsize = 0
    if pos_imsize == neg_imsize:
        imsize = pos_imsize
    else:
        # Should throw error for datasets not having same image size
        pass

    print("Image Size: ", imsize)

    train_data, val_data, train_labels, val_labels, train_weights, val_weights = merge_and_split_data(pos_image_data,
                                                                                                      neg_image_data)


    #print(train_labels)
    #print(val_labels)
    #print(val_weights)

    print("Merged and Split Training Data")
    print("Training Data Size: ", len(train_data))
    print("Validation Data Size: ", len(val_data))

    x = tf.placeholder('float', shape=[None, imsize, imsize])
    answer = tf.placeholder('float', shape=[None, 2])
    is_training = tf.placeholder(tf.bool, shape=None)
    keep_prob = tf.placeholder(tf.float32)
    weights = tf.placeholder('float', shape=[None, 1])

    (train_step, loss, accuracy, summaries) = setup_model(x, answer, imsize, is_training, keep_prob, weights)
    train_summary, val_summary = summaries
    init = tf.global_variables_initializer()
    print("Setup Model")

    num_epochs = 200

    log_dir = options.tb_log_dir

    with tf.device('/gpu:0'):
        with tf.Session() as sess:

            train_writer = tf.summary.FileWriter(log_dir + "/train", sess.graph)
            val_writer = tf.summary.FileWriter(log_dir + "/val", sess.graph)

            sess.run(init)

            for t in range(num_epochs):
                print("EPOCH: ", str(t + 1))
                start = time.time()
                avg_train_loss, train_acc = process_train_data(train_data,
                                                               train_labels,
                                                               train_weights,
                                                               x, answer, is_training,
                                                               keep_prob, weights,
                                                               loss, accuracy,
                                                               train_step, sess,
                                                               train_summary, train_writer)
                end = time.time()

                avg_val_loss, val_acc = process_val_data(val_data,
                                                         val_labels,
                                                         val_weights,
                                                         x, answer,
                                                         is_training, keep_prob, weights,
                                                         accuracy, loss, sess,
                                                         val_summary, val_writer)

                print("train_loss: " + str(avg_train_loss) + " val_loss: " + str(avg_val_loss))
                print("train_acc: " + str(train_acc))
                print("val_acc: " + str(val_acc))
                print("Training Time: ", str(end - start))

                # Permute data each epoch
                train_data, train_labels, train_weights = permute_train_data(train_data, train_labels, train_weights)


options = options_parser.setup_data_options()
print(options)

train_batch_count = 0
val_batch_count = 0

if __name__ == "__main__":
    main(options)
