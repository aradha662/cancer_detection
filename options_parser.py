import argparse

def setup_data_options():
    options = argparse.ArgumentParser()

    options.add_argument('-c', action="store", dest='pos_label_dir',
                         default='pos_labels')

    options.add_argument('-n', action="store", dest='neg_label_dir',
                         default='neg_labels')

    options.add_argument('-l', action="store", dest='tb_log_dir',
                         default='tensorboard_logs')

    return options.parse_args()

def setup_plotter_options():
    options = argparse.ArgumentParser()

    options.add_argument('-i', action="store", dest='input_fname',
                         default='logs/log.txt')

    options.add_argument('-o', action="store", dest='output_dir_name',
                         default='plots/')

    options.add_argument('-n', action="store", dest='num_epochs',
                         default=-1, type=int)

    options.add_argument('-s', action="store", dest='start_epoch',
                         default=0, type=int)

    return options.parse_args()
