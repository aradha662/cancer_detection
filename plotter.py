import matplotlib
matplotlib.use('Pdf')
from  matplotlib import pyplot as plt
import options_parser

def main(options):
    input_fname = options.input_fname
    output_dir_name = options.output_dir_name
    num_epochs = options.num_epochs
    start_epoch = options.start_epoch

    epochs = []
    train_losses = []
    val_losses = []
    train_acc = []
    val_acc = []
    
    with open(input_fname, 'r') as f:
        use_next = 0
        for line in f:
            line = line.strip()
            if len(line) <= 5:
                continue
            elif line[0:5] == "EPOCH":
                tokens = line.split()
                epochs.append(int(tokens[1]))
                use_next = 3
            elif use_next > 0:
                tokens = line.split()
                if use_next == 3 and len(tokens) == 4:
                    train_losses.append(float(tokens[1]))
                    val_losses.append(float(tokens[3]))
                    use_next -= 1
                elif use_next == 2 and len(tokens) == 2:
                    train_acc.append(float(tokens[1]))
                    use_next -= 1
                elif use_next == 1 and len(tokens) == 2:
                    val_acc.append(float(tokens[1]))
                    use_next -= 1
                    if len(epochs) > num_epochs and num_epochs > 0:
                        break
                        
        # If printing at intermediate step
        min_length = min(len(epochs), len(train_losses),
                         len(val_losses),len(train_acc),
                         len(val_acc))

        epochs = epochs[start_epoch:min_length]
        train_losses = train_losses[start_epoch:min_length]
        val_losses = val_losses[start_epoch:min_length]
        train_acc = train_acc[start_epoch:min_length]
        val_acc = val_acc[start_epoch:min_length]

        plot2_2D(epochs, train_losses, epochs, val_losses,
                 "Epochs", "Softmax Loss", "Naive Model Loss",
                 ["Train_Loss", "Val_Loss"],
                 output_dir_name + "loss.pdf", 
                 legend_location="upper right")
        
        plot2_2D(epochs, train_acc, epochs, val_acc,
                 "Epochs", "Accuracy", "Naive Model Accuracy",
                 ["Train_Acc", "Val_Acc"],
                 output_dir_name + "acc.pdf",
                 legend_location="lower right")

    
def plot2_2D(x1, y1, x2, y2, x_label, y_label, 
             title, legend_list, 
             fname, scatter=False, savefig=True,
             legend_location="lower right"):

    fig = plt.figure()
    ax = fig.add_subplot(111)
    if not scatter:
        ax.plot(x1, y1, '-ro', label=legend_list[0])
    else:
        ax.plot(x1, y1, 'ro', label=legend_list[0])

    if not scatter:
        ax.plot(x2, y2, '--bo', label=legend_list[1])
    else:
        ax.plot(x2, y2, 'bo', label=legend_list[1])

    plt.xlabel(x_label)
    plt.ylabel(y_label)

    xticks, xticklabels = plt.xticks()
    # shift half a step to the left
    xmin = (3*xticks[0] - xticks[1])/2.
    # shift half a step to the right
    xmax = (3*xticks[-1] - xticks[-2])/2.

    yticks, yticklabels = plt.yticks()
    # shift half a step to the left
    ymin = (3*yticks[0] - yticks[1])/2.
    # shift half a step to the right
    ymax = (3*yticks[-1] - yticks[-2])/2.
    plt.xlim(xmin, xmax)
    plt.ylim(ymin, ymax)
    plt.title(title)

    legend = ax.legend(loc=legend_location, shadow=True)

    if savefig:
        fig.savefig(fname)
  
          
options = options_parser.setup_plotter_options()
if __name__ == "__main__":
    main(options)


